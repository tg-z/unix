# Tags

Tag a Repository
----------------

```
$ limo tag limo cli git
hoop33/limo (★ : 6) (Go)
Added tag 'cli'
Added tag 'git'
```

Untag a Repository
------------------

```
$ limo untag limo cli
hoop33/limo (★ : 6) (Go)
Removed tag 'cli'
```

Create a Tag
------------

```
$ limo add tag cli
Created tag 'cli'
```

```
$ limo add tag utility nosql
Created tag 'utility'
Created tag 'nosql'
```

Delete a Tag
------------

```
$ limo delete nosql
Deleted tag 'nosql'
```

```
$ limo rm nosql
Deleted tag 'nosql'
```

Rename a Tag
------------

```
$ limo rename aweosme awesome
```

```
$ limo mv aweosme awesome
```

```
$ limo list tags
awesome
cli
```

```
$ limo ls tags
awesome
cli
```

List All Repositories for a Tag
-------------------------------

```
$ limo list stars --tag cli
hoop33/limo (★ : 6) (Go)
jonas/tig (★ : 3241) (C)
``` 
