# Trending

**Note**: Trending currently works only with GitHub.

You can see what projects are currently trending on GitHub but using the `list trending` command:
```
$ limo list trending
...
```

You can see results only for a specific language using the `--language` flag:
```
$ limo list trending --language go
...
```

The results don't exactly match the GitHub trending page [https://github.com/trending](https://github.com/trending). The `list trending` command shows the trending repositories created within the past seven days, sorted by star count.
