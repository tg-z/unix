# Stars

Update Stars
------------

```
$ limo update --service github
Updating . . . |
Created: 4; Updated: 633; Errors: 0
```

List Stars
----------

```
$ limo list stars
...
jaxbot/github-issues.vim (VimL)
jaxbot/semantic-highlight.vim (VimL)
```

```
$ limo ls stars
...
jaxbot/github-issues.vim (VimL)
jaxbot/semantic-highlight.vim (VimL)
```

```
$ limo list stars --language go
...
hoop33/limo (★ : 6) (Go)
...
```

```
$ limo list stars --tag cli
hoop33/limo (★ : 6) (Go)
jonas/tig (★ : 3241) (C)
```

View Detail on a Star
---------------------

```
$ limo show limo
hoop33/limo (★ : 6) (Go)
cli, git, github, gitlab, bitbucket
A CLI for managing starred Git repositories
Home page: https://www.gitbook.com/book/hoop33/limo/details
URL: https://github.com/hoop33/limo.git
Starred on Tue Jun 21 14:56:05 UTC 2016
```

Open a Star in Your Browser
---------------------------

```
$ limo open limo
Opening https://github.com/hoop33/limo.git...
```

```
$ limo open limo --homepage
Opening https://www.gitbook.com/book/hoop33/limo/details...
``` 
 
