# Search

`limo` brings the power of full-text search to your starred repositories. You can search across repository full names, names, descriptions, tags, and languages. To search your repositories, use the search command. For example, to search for databases written in Go, you could type:

```
$ limo search database go
(0.531617) vimrus/tickdb (★ : 20) (Go)
(0.526717) cayleygraph/cayley (★ : 8382) (Go)
(0.517311) dgraph-io/dgraph (★ : 1577) (Go)
(0.517311) prometheus/prometheus (★ : 5158) (Go)
(0.517311) SoftwareDefinedBuildings/btrdb (★ : 237) (Go)
(0.512793) lafikl/pginsight (★ : 178) (Go)
(0.220367) mongodb/mongo (★ : 9566) (C++)
(0.218787) rethinkdb/rethinkdb (★ : 14480) (C++)
(0.211840) haifengl/unicorn (★ : 129) (Scala)
(0.211840) OmniDB/OmniDB (★ : 537) (JavaScript)
```
The score on the left indicates how well the repository matched your query. The higher the score, the better the match.
Look for the search capabilities to grow over time.

`limo`  uses the [Bleve](http://www.blevesearch.com/) search engine, so all of the [query language](http://www.blevesearch.com/docs/Query-String-Query/) should work. I haven't yet spent enough time to determine whether I have this working properly yet. For reference, the currently-mapped field names are:

*   Name
*   FullName
*   Description
*   Language
*   Tags.Name
