# Usage

```
$ limo list stars
```

| Command | Description |
| --- | --- |
| `add star <URL> [tag]...` | Star repository at `URL` and optionally add tags |
| `add tag <tag>` | Add tag `tag` |
| `delete star <URL/name>` | Unstar repository at `URL` or `name` |
| `delete tag <tag>` | Delete tag `tag` |
| `help [command]` | Show help for `command` or leave blank for all commands |
| `list events [--count <num>] [--page <num>]` | List events (e.g., what you see on your GitHub home page) |
| `list languages` | List languages |
| `list stars [--tag <tag>]... [--language <language>]... [--service <service>]... [--user <user>] [--output <output>]` | List starred repositories |
| `list tags [--output <output>]` | List tags |
| `list trending [--language <language>]... [--service <service>]... [--output output]` | List trending repositories |
| `login [--service <service>]` | Logs in to a service (default: github) |
| `open <star>` | Opens the URL of a star in your default browser |
| `prune [--dryrun]` | Prunes unstarred items from your local database |
| `rename <tag> <new>` | Rename tag `tag` to `new` |
| `show <star> [--output <output>]` | Show repository details |
| `search <search string> [--output <output>]` | Search your stars |
| `tag <star> <tag>...` | Tag starred repository `star` using tags `tag` |
| `untag <star> <tag>...` | Untag starred repository `star` using `tag` or leave blank for all tags |
| `update [--service <service>] [--user <user>]` | Update `service` or leave blank for all services for user `user` or leave blank for you |
| `version` | Show program version |

Flags
-----

| Flag | Description |
| --- | --- |
| `-l, --language <language>` | The computer language |
| `-o, --output <color/text/csv/json>` | The output format |
| `-s, --service <github/gitlab/bitbucket>` | The service |
| `-t, --tag <name>` | The tag name |
| `-u, --user <user>` | The user |
| `-v, --verbose` | Turn on verbose output | 

