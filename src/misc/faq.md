# FAQ

*   Why the name "limo"?
    *   If you know anything about Hollywood, you know that limos carry . . . stars.
*   Where is this information stored?
    *   The configuration is stored in `~/.config/limo`. Inside that directory, you'll find:
        *   `limo.yaml`: Configuration information
        *   `limo.db`: The SQLite database that stores all your stars and tags
        *   `limo.idx`: The Bleve search index

