# Limo Introduction

Limo searches, tags, and displays your starred repositories. It currently works with
GitHub and GitLab, and Bitbucket integration is planned.

The source code is available at https://github.com/hoop33/limo and contributions are welcome.

[Getting Started](README.md)
[Usage](usage/README.md)
- [Stars](usage/stars.md)
- [Tags](usage/tags.md)
- [Search](usage/search.md)
- [Trending](usage/trending.md)
[Misc](misc/README.md)
- [Credits](misc/credits.md)
- [FAQ](misc/faq.md)
