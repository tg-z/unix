# Getting Started

Installation
------------

If you have a working [Go](https://golang.org/) installation, you can type:

```
$ go get -u github.com/hoop33/limo
```

Note that `limo` has been tested with Go 1.7. Other Go versions will likely work as well.

You can also download a pre-built binary for your platform and copy it to a location on your path.

*   [macOS](https://github.com/hoop33/limo/raw/master/dist/macos/limo)
    
*   [Linux](https://github.com/hoop33/limo/raw/master/dist/linux/limo)
    
*   [Windows](https://github.com/hoop33/limo/raw/master/dist/windows/limo.exe)
    

Log In to GitHub
----------------

`limo` uses a GitHub API key, so first [create one](https://github.com/blog/1509-personal-api-tokens). Then, log in:
```
$ limo login
```

When prompted, paste your token and hit enter.

Log In to GitLab
----------------

`limo` uses a GitLab Personal Access Token, so log in to GitLab, go to your profile, and create one. Then, log in:
```
$ limo login --service gitlab
```

When prompted, paste your token and hit enter.

Update from GitHub and GitLab
-----------------------------

To update from GitHub, just type:

```
$ limo update
```

```
$ limo update --service gitlab
```

That's it! You know have a local copy of your GitHub and GitLab starred repositories, ready for you to search, tag, list, and view! 

